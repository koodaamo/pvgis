import os, importlib
from decimal import Decimal
from .. import ParametrizedTestCase
import mock
import requests
import unittest

# api to test
from pvgis.util import address_to_coordinates


class GeoCodingTestCase(ParametrizedTestCase):

    def setUp(self):

        # read json files
        curdir = os.path.dirname(self.module.__file__)
        with open(curdir + os.sep + self.module.__name__.split('.')[-1] + ".json") as jsonfile:
           self.geocoding_result_json = jsonfile.read()

        # mock the response
        r = requests.Response()
        r.status_code = requests.codes.ok
        r._content = self.geocoding_result_json
        r.encoding = "utf-8"
        requests.get = mock.Mock(return_value=r)

        # get expected results


    def test_api(self):
        "does geocoding API work?"
        m = self.module
        place, (lat, lng) = address_to_coordinates(m.test_addr)
        self.assertEqual(place, m.result_place)
        self.assertEqual(lat, m.result_lat)
        self.assertEqual(lng, m.result_lng)


# collect data files to the suite
suite = unittest.TestSuite()
cases = ("laserkatu_10_53850_lappeenranta", "utsjoki")
for case in cases:
    suite.addTest(ParametrizedTestCase.parametrize(GeoCodingTestCase, "tests.geocoding." + case))
