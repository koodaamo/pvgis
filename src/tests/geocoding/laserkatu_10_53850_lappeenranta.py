from decimal import Decimal

# test data
test_addr = "Laserkatu 10, Lappeenranta"

# expected results
result_place = "Laserkatu 10, 53850 Lappeenranta, Finland"
result_lat = Decimal("61.06489029999999")
result_lng = Decimal("28.09481040")
