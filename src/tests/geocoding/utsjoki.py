from decimal import Decimal

# test data
test_addr = "Utsjoki"

# expected results
result_place = "Utsjoki, Finland"
result_lat = Decimal("69.90904650")
result_lng = Decimal("27.02852970")
