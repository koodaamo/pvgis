import os, importlib
from decimal import Decimal
from .. import ParametrizedTestCase
import mock
import requests
import unittest

# api to test
from pvgis import estimate_production
from pvgis.eu_pvgis_wrapper import parse_losses
from pvgis.eu_pvgis_wrapper import parse_production_and_irradiation
from pvgis.util import address_to_coordinates


class EstimationTestCase(ParametrizedTestCase):

    def setUp(self):

        # read html file
        curdir = os.path.dirname(self.module.__file__)
        with open(curdir + os.sep + self.module.__name__.split('.')[-1] + ".html") as htmlfile:
           self.estimation_result_html = htmlfile.read()

        # mock the eu pvgis post response
        p = requests.Response()
        p.status_code = requests.codes.ok
        p._content = self.estimation_result_html
        p.encoding = "utf-8"
        requests.post = mock.Mock(return_value=p)

        # mock the geocoding get response
        g = requests.Response()
        g.status_code = requests.codes.ok
        with open(curdir + os.sep + self.module.__name__.split('.')[-1] + ".json") as jsonfile:
           geocoding_result_json = jsonfile.read()
        g._content = geocoding_result_json
        g.encoding = "utf-8"
        requests.get = mock.Mock(return_value=g)

        # get expected results
        self.irradiation = self.module.irradiation
        self.production = self.module.production
        self.losses = self.module.losses


    def test_parsing(self):
        "does parsing of responses work?"
  
        parsed_losses = parse_losses(self.estimation_result_html)
        assert(parsed_losses==self.losses)

        (parsed_prod, parsed_irr) = parse_production_and_irradiation(self.estimation_result_html)
        assert(parsed_irr==self.irradiation)
        assert(parsed_prod==self.production)


    def test_api(self):
        "does estimation API work?"
        m = self.module
        place, (lat, lng) = address_to_coordinates(m.test_addr)
        result = estimate_production(m.test_power, lat, lng, m.test_direction, m.test_angle)
        assert(result==self.production)


# collect data files to the suite
suite = unittest.TestSuite()
cases = ("lappeenranta_p5l14s35a180",)
for case in cases:
    suite.addTest(ParametrizedTestCase.parametrize(EstimationTestCase, "tests.estimation." + case))
