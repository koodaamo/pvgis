import unittest
from estimation import suite as estimation_test_suite
from geocoding import suite as geocoding_test_suite
def test_suite():
   return unittest.TestSuite((estimation_test_suite, geocoding_test_suite))
