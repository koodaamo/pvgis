"""pvgis - tool for solar power calculations

Usage:
  pvgis  <power> <address> <direction> <angle> [--losses=<percentage>]
  pvgis  <power> <latitude> <longitude> <direction> <angle> [--losses=<percentage>]
  pvgis (-h | --help)
  pvgis --version

Options:
  -h --help             Show this screen
  --version             Show version
  --losses=<percentage> Losses from inverter, cabling etc.

Note that address also accepts just a city.

"""

import sys
from docopt import docopt
from api import estimate_production
from util import address_to_coordinates

def calculate_pv_energy():

   if len(sys.argv) == 1:
      sys.exit(__doc__)

   args = docopt(__doc__, version='pvgis 1.0')
   if args["<address>"]:
      place, (lat, lng) = address_to_coordinates(args["<address>"])
      print "\nUsing geocoded coordinates for %s." % place
   else:
      lat = args["<lat>"]
      lng = args["<lng>"]

   losses = args.get("--losses")

   result = estimate_production(args["<power>"], lat, lng, args["<direction>"], args["<angle>"], losses=losses)
   print "Estimated energy production: %i kWh/year" % result["tot"]
