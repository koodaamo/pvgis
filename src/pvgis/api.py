"""the api delegates to various implementations; for now, the eu pvgis"""

from eu_pvgis_wrapper import process


def estimate_production(power, lat, lng, direction, angle, losses=None):
   "estimate solar plant production, in kWh"
   result = process(power, lat, lng, direction, angle, losses=losses)
   return result["production"]


def estimate_losses(power, lat, lng, direction, angle):
   "estimate solar system losses"
   result = process(power, lat, lng, direction, angle, losses=None)
   return result["losses"]


def estimate_irradiation(power, lat, lng, direction, angle, losses=None):
   "estimate solar irradiation, in kWh/m2"
   result = process(power, lat, lng, direction, angle, losses=losses)
   return result["irradiation"]

