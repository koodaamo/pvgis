import ConfigParser, copy

def merge_defaults(cfg_filepth, defaults):
   "utility to merge hardwired defaults & defaults.ini file"
   config = ConfigParser.ConfigParser()
   config.read(cfg_filepth)
   merged = copy.deepcopy(defaults)
   for s in config.sections():
      merged.update(dict(config.items(s)))
   return merged
